import sun.applet.AppletClassLoader;
import sun.plugin2.applet.Applet2ClassLoader;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.stream.Collectors;

public class PluginManager {
    private final String pluginRootDirectory;

    public PluginManager(String pluginRootDirectory) {
        this.pluginRootDirectory = pluginRootDirectory;
    }

    public List<Plugin> load(String pluginClassName) throws ClassNotFoundException, InstantiationException, IllegalAccessException, IOException {
        List<Plugin> pluginList = new ArrayList<>();

        List<File> fileList = Files.walk(Paths.get(pluginRootDirectory))
                .filter(Files::isRegularFile)
                .map(Path::toFile)
                .collect(Collectors.toList());

        for (File file : fileList) {
            JarFile jarFile = new JarFile(file);
            Enumeration<JarEntry> jarEntryEnumeration = jarFile.entries();

            URL[] urls = new URL[]{new URL("jar:file:" + jarFile.getName() + "!/")};

            URLClassLoader urlClassLoader = URLClassLoader.newInstance(urls);

            while (jarEntryEnumeration.hasMoreElements()) {
                JarEntry jarEntry = jarEntryEnumeration.nextElement();
                if (jarEntry.getName().equals(pluginClassName + ".class")) {
                     Class<?> clazz = urlClassLoader.loadClass(pluginClassName);
                    Plugin plugin = (Plugin) clazz.newInstance();
                    pluginList.add(plugin);
                }
            }
        }
        return pluginList;
    }
}
